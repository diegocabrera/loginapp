import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoginModel } from "../models/login.model";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private url = `http://localhost:${3000}/api/`;
  userToken: string;

  constructor(private http: HttpClient) {}

  login(usuarioLogueado: LoginModel) {
    let login = {
      ...usuarioLogueado
    };
    return this.http.post(`${this.url}login`, login).pipe(
      map(respuesta => {
        this.tokenStorage(respuesta["token"]);
        return respuesta;
      })
    );
  }

  private tokenStorage(token: string) {
    this.userToken = token;
    localStorage.setItem("token", token);
  }
}
