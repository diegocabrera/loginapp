import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { LoginModel } from '../../models/login.model';
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  logins: LoginModel = new LoginModel()

  constructor(private auth: AuthService) { }

  ngOnInit() {
  }

  logueo(form: NgForm){
    if(form.invalid){
      return;
    }

    Swal.fire({
			allowOutsideClick: false,
			type: "info",
      text: "Espere un momento por favor...",
		});
		Swal.showLoading();

    this.auth.login(this.logins).subscribe( respuesta => {
      Swal.fire({
        allowOutsideClick: false,
        type: "success",
        text: "¡Listo!",
      });
    }, e => {
      let err;
				switch (e.error.err.message) {
					case "User no found":
						err = "Usuario invalido";
						break;
					case "Password no found":
						err = "La contraseña es incorrecta";
						break;
					case "USER_DISABLED":
						err =
							"El usuario ha sido deshabilitado por el Administrador";
						break;
					default:
						break;
				}
				Swal.fire({
					type: "error",
					title: "Error en la autentificación",
					text: err
				});
    })
  }

}
