require('./backend/Config/db')

const cors = require("cors");
const express = require('express');
const app = express()
const bodyParser = require('body-parser');
const api = require('./backend/Routes');

app.use(bodyParser.urlencoded({ extended: true }))

app.use(bodyParser.json());

app.use(cors());

app.use('/api', api);


async function run() {
    await app.listen(3000)
    console.log('Server corriendo en el puerto 3000');
}

run();