require('../Config');
const bcrypt = require("bcrypt");
const _ = require("underscore");
const jwt = require("jsonwebtoken");

const Users = require('../Models/User.model');


let GetUsers = async(req, res) => {
    let users = await Users.find()
    res.json({
        ok: true,
        users
    })
}


let Login = (req, res) => {
    let body = req.body;

    Users.findOne({ username: body.user }, (err, user) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!user) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "User no found"
                }
            });
        }

        if (!bcrypt.compareSync(body.password, user.password)) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: "Password no found"
                }
            });
        }

        let token = jwt.sign({
                user
            },
            process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN }
        );

        res.json({
            ok: true,
            user,
            token
        });
    });

}

let Registro = (req, res) => {
    let body = _.pick(req.body, ["username", "email"]);
    body.password = bcrypt.hashSync(req.body.password, 5);

    let newUser = new Users(body)

    newUser.save((err, user) => {
        if (err) {
            res.status(500).json({
                ok: false,
                err
            })
        }

        res.json({
            ok: true,
            user
        })
    })


}







module.exports = {
    Login,
    Registro,
    GetUsers
}