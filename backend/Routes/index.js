const express = require('express');
const api = express.Router();

const { verifyAdmin, verifyToken } = require('../Middleware/Auth.middleware')

const { Login, Registro, GetUsers } = require('../Controllers/Login.controller');


api.post('/login', Login);
api.post('/registro', Registro);
api.get('/users', [verifyToken], GetUsers);

module.exports = api;